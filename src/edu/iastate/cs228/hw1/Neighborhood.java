/**
 *
 */
package edu.iastate.cs228.hw1;

/**
 * Neighborhood
 *
 * Usage: Helper classes used to perform various operations for on plain
 * neighborhoods. This class should be considered disposable for each new cycle.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190127
 *
 */
class Neighborhood {

    /**
     * Purpose: State Information
     */
    private final Plain m_plain;
    private final int   INDEX_ROW = 0;
    private final int   INDEX_COL = 1;
    private final int[] m_arrMinPos;
    private final int[] m_arrMaxPos;
    private final int   m_iWidth;
    private final int   m_iLength;

    /**
     * Purpose: Constructor Method
     *
     * @param p
     * @param arrMin
     * @param arrMax
     */
    public Neighborhood( Plain p, int[] arrMin, int[] arrMax ) {
        this.m_plain = p;
        this.m_arrMinPos = arrMin;
        this.m_arrMaxPos = arrMax;

        // Plus one to account for the 0th index.
        this.m_iLength = arrMax [this.INDEX_ROW] - arrMin [this.INDEX_ROW] + 1;
        this.m_iWidth = arrMax [this.INDEX_COL] - arrMin [this.INDEX_COL] + 1;
    }

    /**
     * Returns the minimum vector of the neighborhood.
     *
     * @return Minimum Vector
     */
    public int[] GetMin () {
        return this.m_arrMinPos;
    }

    /**
     * Return a specific minimum position of the minimum vector.
     *
     * @param index
     * @return Minimum Position
     */
    public int GetMin (int index) {
        return this.m_arrMinPos [index];
    }

    /**
     * Returns the maximum vector of the neighborhood.
     *
     * @return Maximum Vector
     */
    public int[] GetMax () {
        return this.m_arrMaxPos;
    }

    /**
     * Returns a specific maximum position of the maximum vector.
     *
     * @param index
     * @return Maximum Position
     */
    public int GetMax (int index) {
        return this.m_arrMaxPos [index];
    }

    /**
     * Returns a partition of a grid where the neighborhood resides.
     *
     * @return Partitioned Grid
     */
    public Living[][] GetContents () {
        // Adding one because we index at 0 :(
        final Living[][] arrNeighborhood = new Living [this.m_iLength] [this.m_iWidth];

        for (int iRow = this.GetMin (this.INDEX_ROW); iRow < this.GetMax (this.INDEX_ROW) + 1; iRow ++)
            for (int iCol = this.GetMin (this.INDEX_COL); iCol < this.GetMax (this.INDEX_COL) + 1; iCol ++)
                arrNeighborhood [iRow - this.GetMin (this.INDEX_ROW)] [iCol
                        - this.GetMin (this.INDEX_COL)] = this.m_plain.grid [iRow] [iCol];
        return arrNeighborhood;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString () {
        return Serialization.Export (this.GetContents ());
    }

    /**
     * Used to determine of a object exists inside of the neighborhood.
     *
     * @param thing
     * @return Is Residence
     */
    public boolean IsResident (Living thing) {
        // If the neighborhood is [0, 0, 2, 2]
        // The input's position must be somewhere in the area.

        return thing.row <= this.GetMax (this.INDEX_ROW) && thing.column <= this.GetMax (this.INDEX_COL)
                && thing.row >= this.GetMin (this.INDEX_ROW) && thing.column >= this.GetMin (this.INDEX_COL);
    }

}
