package edu.iastate.cs228.hw1;

/**
 * Empty
 *
 * Usage: Empty squares are competed by various forms of life.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class Empty extends Living {

    /**
     * Purpose: Constructor Method
     *
     * @param p
     * @param r
     * @param c
     */
    public Empty( Plain p, int r, int c ) {
        super (p, r, c);
    }

    /*
     * (non-Javadoc)
     *
     * @see edu.iastate.cs228.hw1.Living#who()
     */
    @Override
    public State who () {
        return State.EMPTY;
    }

    /**
     * An empty square will be occupied by a nei ghboring Badger, Fox, Rabbit, or
     * Grass, or remain empty.
     *
     * @param pNew plain of the next life cycle.
     * @return Living life form in the next cycle.
     */
    @Override
    public Living next (Plain pNew) {

        final int[] arrResults = this.CensusArray ();
        this.census (arrResults);

        if (arrResults [RABBIT] > MIN_NEIGHBOR)
            return new Rabbit (pNew, this.row, this.column, 0);

        if (arrResults [FOX] > MIN_NEIGHBOR)
            return new Fox (pNew, this.row, this.column, 0);

        if (arrResults [BADGER] > MIN_NEIGHBOR)
            return new Badger (pNew, this.row, this.column, 0);

        if (arrResults [GRASS] >= MIN_NEIGHBOR)
            return new Grass (pNew, this.row, this.column);

        return new Empty (pNew, this.row, this.column);
    }
}
