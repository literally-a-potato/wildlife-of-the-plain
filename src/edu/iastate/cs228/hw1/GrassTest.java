/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * GrassTest
 *
 * Usage: Various tests for the Grass class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class GrassTest {

    @Test
    public void TestbedOvergrazing () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-GrassA.txt");
        final Living thing = p.grid [1] [1].next (p);

        assertEquals ("The grass should be all gone, there were too many rabbits.", State.EMPTY, thing.who ());
    }

    @Test
    public void TestbedEatenByRabbit () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-GrassB.txt");
        final Living thing = p.grid [1] [1].next (p);

        assertEquals ("The grass should be replaced by a rabbit.", State.RABBIT, thing.who ());
    }
}
