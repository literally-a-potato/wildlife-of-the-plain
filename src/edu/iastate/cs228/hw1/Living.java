package edu.iastate.cs228.hw1;

/**
 * Living
 *
 * Usage: Living refers to the life form occupying a square in a plain grid. It
 * is a superclass of Empty, Grass, and Animal, the latter of which is in turn a
 * superclass of Badger, Fox, and Rabbit. Living has two abstract methods
 * awaiting implementation.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public abstract class Living {
    protected Plain plain;  // the plain in which the life form resides
    protected int   row;    // location of the square on which
    protected int   column; // the life form resides

    // constants to be used as indices.
    protected static final int BADGER = 0;
    protected static final int EMPTY  = 1;
    protected static final int FOX    = 2;
    protected static final int GRASS  = 3;
    protected static final int RABBIT = 4;

    // Constants used by child classes.
    public static final int MIN_NEIGHBOR   = 1;
    public static final int NUM_LIFE_FORMS = 5;
    public static final int BADGER_MAX_AGE = 4;
    public static final int FOX_MAX_AGE    = 6;

    public static final int RABBIT_MAX_AGE = 3;

    /**
     * Purpose: Constructor Method
     *
     * @param p
     * @param r
     * @param c
     */
    Living( Plain p, int r, int c ) {
        this.plain = p;
        this.row = r;
        this.column = c;
    }

    /**
     * Allocates a 5-slot array for use by Census.
     *
     * @return Allocated Array
     */
    protected int[] CensusArray () {
        return new int [5];
    }

    /**
     * Censuses all life forms in the local neighborhood in a plain.
     *
     * @param population counts of all life forms
     */
    protected void census (int[] population) {
        for (final Living[] arrRow : this.GetLocalNeighborhood ().GetContents ())
            for (final Living thing : arrRow)
                switch (thing.who ()) {
                    case BADGER:
                        population [BADGER] ++;
                        break;
                    case EMPTY:
                        population [EMPTY] ++;
                        break;
                    case FOX:
                        population [FOX] ++;
                        break;
                    case GRASS:
                        population [GRASS] ++;
                        break;
                    case RABBIT:
                        population [RABBIT] ++;
                        break;
                    default:
                        break; // ?? UMMM
                }
    }

    /**
     * Gets the identity of the life form on the square.
     *
     * @return State
     */
    public abstract State who ();
    // To be implemented in each class of Badger, Empty, Fox, Grass, and Rabbit.
    //
    // There are five states given in State.java. Include the prefix State in
    // the return value, e.g., return State.Fox instead of Fox.

    /**
     * Determines the life form on the square in the next cycle.
     *
     * @param pNew plain of the next cycle
     * @return Living
     */
    abstract public Living next (Plain pNew);

    private Neighborhood GetLocalNeighborhood () {
        return this.plain.GetLocalNeighborhood (this);
    }

}
