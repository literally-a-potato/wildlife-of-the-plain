package edu.iastate.cs228.hw1;

/**
 * MyAge
 *
 * Usage: To be implemented by the Animal class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public interface MyAge {
    int myAge (); // return age of the animal
}
