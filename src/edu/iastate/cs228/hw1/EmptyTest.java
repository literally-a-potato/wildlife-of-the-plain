/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * EmptyTest
 *
 * Usage: Various tests for the Empty class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class EmptyTest {
    @Test
    public void TestbedRabbitReplace () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-EmptyA.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The empty space should be replaced by a rabbit.", State.RABBIT, thing.who ());
    }

    @Test
    public void TestbedFoxReplace () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-EmptyB.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The empty space should be replaced by a fox.", State.FOX, thing.who ());
    }

    @Test
    public void TestbedBadgerReplace () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-EmptyC.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The empty space should be replaced by a badger.", State.BADGER, thing.who ());
    }

    @Test
    public void TestbedGrassReplace () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-EmptyD.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The empty space should be replaced by grass.", State.GRASS, thing.who ());
    }
}
