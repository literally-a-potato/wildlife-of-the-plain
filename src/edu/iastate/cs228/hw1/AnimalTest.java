/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * AnimalTest
 *
 * Usage: Various tests fof the Animal class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class AnimalTest {
    @Test
    public void TestbedAgeBadger () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-AllRabbits.txt");
        Living thing = new Badger (p, 1, 1, 0);
        p.grid [1] [1] = thing;

        for (int iAge = 0; iAge < Living.BADGER_MAX_AGE; iAge ++) {
            assertEquals ("myAge () should return " + iAge, iAge, ((Animal) thing).myAge ());
            thing = thing.next (p);
        }

    }
}
