/**
 *
 */
package edu.iastate.cs228.hw1;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.Scanner;

import org.junit.Test;

/**
 * WildlifeTest
 *
 * Usage: Various tests for the Wildlife class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class WildlifeTest {

    private void input (String data) {
        data += "\r\n";
        final InputStream stdin = System.in;
        try {
            System.setIn (new ByteArrayInputStream (data.getBytes ()));
            final Scanner scanner = new Scanner (System.in);
            System.out.println (scanner.nextLine ());
        } finally {
            System.setIn (stdin);
        }
    }

    @Test
    public void testRandom () {
        Wildlife.Action_MenuRandomGeneration ();
        this.input ("3");
        this.input ("3");
    }

    @Test
    public void testFileLoad () {
        Wildlife.Action_MenuRandomGeneration ();
        this.input ("templates/public1-3x3.txt");
        this.input ("3");
    }

}
