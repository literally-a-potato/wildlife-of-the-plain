/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * FoxTest
 *
 * Usage: Various tests for the Fox class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class FoxTest {

    @Test
    public void TestbedAgeFox () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-AllRabbits.txt");
        Living thing = new Fox (p, 1, 1, 0);
        p.grid [1] [1] = thing;

        thing = thing.next (p);
        assertEquals ("Fox should be one year of age as of now.", 1, ((Animal) thing).myAge ());

        for (int iAge = 0; iAge < Living.FOX_MAX_AGE; iAge ++)
            thing = thing.next (p);

        assertEquals ("The fox has been rendered an empty carcass.", State.EMPTY, thing.who ());
    }

    @Test
    public void TestbedBadgerReplacement () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-FoxB.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The fox should be replaced with a badger.", State.BADGER, thing.who ());
    }

    @Test
    public void TestbedTooManyPreds () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-Medley.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The fox should be rendered an empty carcass.", thing.who (), State.EMPTY);
    }
}
