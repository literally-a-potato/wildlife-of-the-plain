/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * BadgerTest
 *
 * Usage: Various tests for the Badger class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class BadgerTest {

    @Test
    public void TestbedAgeBadger () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-AllRabbits.txt");
        Living thing = new Badger (p, 1, 1, 0);
        p.grid [1] [1] = thing;

        thing = thing.next (p);
        assertEquals ("The badger should be one year of age as of now.", ((Animal) thing).myAge (), 1);

        for (int iAge = 0; iAge < Living.BADGER_MAX_AGE; iAge ++)
            thing = thing.next (p);

        assertEquals ("The badger should be rendered an empty carcass.", State.EMPTY, thing.who ());
    }

    @Test
    public void TestbedFoxReplacement () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-BadgerB.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The badger should be replaced with a fox.", State.FOX, thing.who ());
    }

    @Test
    public void TestbedTooManyPreds () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-Medley.txt");
        final Living thing = p.grid [0] [0].next (p);

        assertEquals ("The badger should be rendered an empty carcass.", State.EMPTY, thing.who ());
    }
}
