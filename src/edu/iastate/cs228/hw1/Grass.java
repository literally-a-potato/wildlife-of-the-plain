package edu.iastate.cs228.hw1;

/**
 * Grass
 *
 * Usage: Grass remains if more than rabbits in the neighborhood; otherwise, it
 * is eaten.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class Grass extends Living {
    /**
     * Purpose: Constructor Method
     *
     * @param p
     * @param r
     * @param c
     */
    public Grass( Plain p, int r, int c ) {
        super (p, r, c);
    }

    /*
     * (non-Javadoc)
     * 
     * @see edu.iastate.cs228.hw1.Living#who()
     */
    @Override
    public State who () {
        return State.GRASS;
    }

    /**
     * Grass can be eaten out by too many rabbits. Rabbits may also multiply fast
     * enough to take over Grass.
     */
    @Override
    public Living next (Plain pNew) {
        final int[] arrResults = this.CensusArray ();
        this.census (arrResults);

        // Rule A
        if (arrResults [RABBIT] * 3 > arrResults [GRASS])
            return new Empty (pNew, this.row, this.column);

        // Rule B
        if (arrResults [RABBIT] >= 3)
            return new Rabbit (pNew, this.row, this.column, 0);

        return new Grass (pNew, this.row, this.column);
    }
}
