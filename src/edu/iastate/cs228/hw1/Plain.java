package edu.iastate.cs228.hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

/**
 * Plain
 *
 * Usage: The plain is represented as a square grid of size width x width.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190125
 *
 */
public class Plain {
    private final static int       NEIGHBORHOOD_SIZE = 3;
    private final int              width;
    private final Neighborhood[][] m_neighborhoods;
    private final Serialization    m_serial;
    public Living[][]              grid;

    /**
     * Default constructor reads from a file
     */

    public Plain( String inputFileName ) throws FileNotFoundException {
        this.m_serial = new Serialization (this);
        final Scanner file = new Scanner (new File (inputFileName));
        final ArrayList<ArrayList<Living>> arrGrid = this.m_serial.FileImport (file);

        this.width = arrGrid.size ();
        this.grid = new Living [this.width] [ ];
        for (int iRow = 0; iRow < this.grid.length; iRow ++) {
            // Allocate row size using ArrayList size.
            final ArrayList<Living> arrLRow = arrGrid.get (iRow);
            this.grid [iRow] = new Living [arrLRow.size ()];
            Arrays.setAll (this.grid [iRow], (index) -> arrLRow.get (index));
        }

        this.m_neighborhoods = this.GenerateNeighborhoods ();
    }

    /**
     * Constructor that builds a w x w grid without initializing it.
     *
     * @param width the grid
     */
    public Plain( int w ) {
        this.width = w;
        this.grid = new Living [this.width] [this.width];
        this.m_neighborhoods = this.GenerateNeighborhoods ();
        this.m_serial = new Serialization (this);
    }

    /**
     * Helper method which returns the current neighborhood the cell is in.
     *
     * @param iRow
     * @param iColumn
     * @return Neighborhood
     */
    protected Neighborhood GetLocalNeighborhood (int iRow, int iColumn) {
        return this.m_neighborhoods [iRow] [iColumn];
    }

    /**
     * Helper method which returns the current neighborhood the cell is in.
     *
     * @param thing
     * @return Neighborhood
     */
    protected Neighborhood GetLocalNeighborhood (Living thing) {
        return this.GetLocalNeighborhood (thing.row, thing.column);
    }

    /**
     * Helper method which is invoked from the constructor to generate the grid's
     * respective neighborhoods.
     *
     * @return 2D Array of Neighborhoods
     */
    protected Neighborhood[][] GenerateNeighborhoods () {
        final int MARGIN_SIZE = NEIGHBORHOOD_SIZE / 2;

        final ArrayList<ArrayList<Neighborhood>> arrLNeighborhoods = new ArrayList<> ();
        for (int iRow = 0; iRow < this.width; iRow ++) {
            final ArrayList<Neighborhood> arrLRow = new ArrayList<> ();
            for (int iCol = 0; iCol < this.width; iCol ++) {
                final int[] arrMinVec = {
                        Math.max (0, iRow - MARGIN_SIZE), Math.max (0, iCol - MARGIN_SIZE)
                };
                final int[] arrMaxVec = {
                        Math.min (this.width - 1, iRow + MARGIN_SIZE), Math.min (this.width - 1, iCol + MARGIN_SIZE)
                };

                arrLRow.add (new Neighborhood (this, arrMinVec, arrMaxVec));
            }
            arrLNeighborhoods.add (arrLRow);
        }

        final Neighborhood[][] arrReturn = new Neighborhood [arrLNeighborhoods.size ()] [ ];
        for (int iRow = 0; iRow < arrLNeighborhoods.size (); iRow ++) {
            final ArrayList<Neighborhood> row = arrLNeighborhoods.get (iRow);
            arrReturn [iRow] = row.toArray (new Neighborhood [row.size ()]);
        }

        return arrReturn;
    }

    /**
     * Returns the width of the grid
     *
     * @return width of the grid
     */
    public int getWidth () {
        return this.width;
    }

    /**
     * Initialize the plain by randomly assigning to every square of the grid one of
     * BADGER, FOX, RABBIT, GRASS, or EMPTY.
     *
     * Every animal starts at age 0.
     */
    private final String m_strSelectables = "BFRGE";

    public void randomInit () {
        final Random generator = new Random ();
        if (this.width == 0)
            return;

        final int iSelectablesBound = this.m_strSelectables.length ();

        this.grid = new Living [this.width] [ ];

        for (int iRow = 0; iRow < this.width; iRow ++) {
            this.grid [iRow] = new Living [this.width];
            for (int iColumn = 0; iColumn < this.width; iColumn ++) {
                final int iSelect = generator.nextInt (iSelectablesBound);
                final char charSelect = this.m_strSelectables.charAt (iSelect);
                this.grid [iRow] [iColumn] = this.m_serial.CharToLiving (charSelect, iRow, iColumn);
            }
        }
    }

    /**
     * Output the plain grid. For each square, output the first letter of the living
     * form occupying the square. If the living form is an animal, then output the
     * age of the animal followed by a blank space; otherwise, output two blanks.
     */
    @Override
    public String toString () {
        return this.m_serial.Export ();
    }

    /**
     * Write the plain grid to an output file. Also useful for saving a randomly
     * generated plain for debugging purpose.
     *
     * @throws FileNotFoundException
     */
    public void write (String outputFileName) throws FileNotFoundException {
        final PrintWriter writer = new PrintWriter (outputFileName);
        final String strOutput = this.toString ();

        writer.write (strOutput);
        writer.close ();
    }
}
