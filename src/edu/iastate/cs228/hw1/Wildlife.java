package edu.iastate.cs228.hw1;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * Wildlife
 *
 * Usage: The Wildlife class performs a simulation of a grid plain with squares
 * inhabited by badgers, foxes, rabbits, grass, or none.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class Wildlife {

    private static int m_iTrialNum = 1;

    /**
     * Update the new plain from the old plain in one cycle.
     *
     * @param pOld old plain
     * @param pNew new plain
     */
    public static void updatePlain (Plain pOld, Plain pNew) {
        final Living[][] arrGrid = pOld.grid;
        for (int iRow = 0; iRow < arrGrid.length; iRow ++)
            for (int iCol = 0; iCol < arrGrid [iRow].length; iCol ++)
                pNew.grid [iRow] [iCol] = arrGrid [iRow] [iCol].next (pNew);
    }

    /**
     * An interface for the user to input desired action
     *
     * @return transition code based on user input.
     */
    private static int menu () {
        boolean bContinue = false;
        System.out.println ("keys: 1 (random plain)\t2 (file input)\t3 (exit)");
        do {
            bContinue = true;

            System.out.printf ("Trial %d: ", Wildlife.m_iTrialNum);

            final Scanner input = new Scanner (System.in);
            final char[] arrChar = input.next ().toCharArray ();

            switch (arrChar [0]) {
                case '1':
                    return 1;
                case '2':
                    return 2;
                case '3':
                    break;
                default:
                    System.out.println ("Please enter a valid key.");
                    bContinue = false;
                    break;
            }

        } while (!bContinue);

        return 0;
    }

    /**
     * Primary method used to collect integers from user input.
     *
     * @param strPrompt
     * @return
     */
    @SuppressWarnings("resource")
    private static int prompt (String strPrompt) {
        do {
            System.out.print (strPrompt);
            final Scanner input = new Scanner (System.in);
            final String strInput = input.next ();

            try {
                return Integer.decode (strInput);
            } catch (final NumberFormatException e) {
                System.out.println ("Please enter a valid integer.");
            }

        } while (true);
    }

    /**
     * Primary method used to collect integers from user input.
     *
     * @param strPrompt
     * @return
     */
    private static String promptFile (String strPrompt) {
        String strInput;

        boolean bContinue = false;
        do {
            bContinue = true;
            System.out.print (strPrompt);
            final Scanner input = new Scanner (System.in);
            strInput = input.nextLine ();

            final File fDisposible = new File (strInput);
            if (!fDisposible.exists ()) {
                bContinue = false;
                System.out.println ("File not found, please enter a correct file.");
            }
        } while (!bContinue);

        return strInput;
    }

    /**
     * Helper method which handles plain iteration cycling and printing results.
     *
     * @param pInitial
     * @param iLastCycle
     */
    private static void Simulate (Plain pInitial, int iLastCycle) {
        Plain pUpdating = pInitial;
        for (int iCycle = 0; iCycle < iLastCycle; iCycle ++) {
            final Plain pNew = new Plain (pInitial.getWidth ());
            updatePlain (pUpdating, pNew);
            pUpdating = pNew;
        }

        System.out.println ("\nInitial Plain:\n\n" + pInitial.toString ());
        System.out.println ("Final Plain:\n\n" + pUpdating.toString ());
    }

    /**
     * Handling a random generation simulation.
     *
     */
    protected static void Action_MenuRandomGeneration () {
        System.out.println ("Random plain");
        final int iWidth = prompt ("Enter grid width: ");
        final int iLastCycle = prompt ("Enter the number of cycles: ");

        final Plain pInitial = new Plain (iWidth);
        pInitial.randomInit ();

        Simulate (pInitial, iLastCycle);
    }

    /**
     * Handling a file input simulation.
     *
     */
    protected static void Action_MenuFileGeneration () {
        System.out.println ("Plain input from a file");
        Plain pInitial = new Plain (0);

        boolean bFile = false;
        do
            try {
                bFile = true;
                final String strFileName = promptFile ("File name: ");
                pInitial = new Plain (strFileName);
            } catch (final FileNotFoundException e) {
                bFile = false;
                System.out.println ("File not found, please enter a correct file.");
            }
        while (!bFile);
        final int iLastCycle = prompt ("Enter the number of cycles: ");

        Simulate (pInitial, iLastCycle);
    }

    /**
     * Repeatedly generates plains either randomly or from reading files. Over each
     * plain, carries out an input number of cycles of evolution.
     *
     * @param args
     * @throws FileNotFoundException
     */
    public static void main (String[] args) throws FileNotFoundException {

        System.out.println ("Simulation of Wildlife of the Plain");

        boolean bContinue = true;
        do {
            switch (menu ()) {
                case 1:
                    Action_MenuRandomGeneration ();
                    break;
                case 2:
                    Action_MenuFileGeneration ();
                    break;
                default:
                    bContinue = false;
                    break;
            }
            ;

            m_iTrialNum ++;
        } while (bContinue);
    }
}
