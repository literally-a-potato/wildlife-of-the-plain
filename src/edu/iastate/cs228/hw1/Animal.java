package edu.iastate.cs228.hw1;

/**
 * Animal
 *
 * Usage: This class is to be extended by the Badger, Fox, and Rabbit classes.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190125
 *
 */
public abstract class Animal extends Living implements MyAge {
    protected int age; // age of the animal

    /**
     * Purpose: Constructor Method
     *
     * @param p
     * @param r
     * @param c
     * @param a
     */
    public Animal( Plain p, int r, int c, int a ) {
        super (p, r, c);
        this.age = a;
    }

    /**
     * Returns the age of an animal.
     *
     * @return age of the animal
     */
    @Override
    public int myAge () {
        return this.age;
    }
}
