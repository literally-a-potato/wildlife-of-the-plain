package edu.iastate.cs228.hw1;

/**
 * Fox
 *
 * Usage: A fox eats rabbits and competes against a badger.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class Fox extends Animal {
    /**
     * Constructor
     *
     * @param p: plain
     * @param r: row position
     * @param c: column position
     * @param a: age
     */
    public Fox( Plain p, int r, int c, int a ) {
        super (p, r, c, a);
    }

    /**
     * A fox occupies the square.
     */
    @Override
    public State who () {
        return State.FOX;
    }

    /**
     * A fox dies of old age or hunger, or from attack by numerically superior
     * badgers.
     *
     * @return Living life form occupying the square in the next cycle.
     */
    @Override
    public Living next (Plain pNew) {
        final int[] arrResults = this.CensusArray ();
        this.census (arrResults);

        // Rule A
        if (this.age >= Living.FOX_MAX_AGE)
            return new Empty (pNew, this.row, this.column);

        // Rule B
        if (arrResults [BADGER] > arrResults [FOX])
            return new Badger (pNew, this.row, this.column, 0);

        // Rule C
        if ((arrResults [BADGER] + arrResults [FOX]) > arrResults [RABBIT])
            return new Empty (pNew, this.row, this.column);

        return new Fox (pNew, this.row, this.column, this.age + 1);
    }
}
