/**
 *
 */
package edu.iastate.cs228.hw1;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * Serialization
 *
 * Usage: A helper class used explicitly for file writing & reading handling.
 * This class deals with serialization and parsing of files for Wildlife of the
 * Plain.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190126
 *
 */
final class Serialization {
    private final Plain m_plain;

    /**
     * Constructor Method
     *
     * @param p
     */
    Serialization( Plain p ) {
        this.m_plain = p;
    }

    /**
     * Converts a living object to it's character representation.
     *
     * @param thing
     * @return Character representation
     */
    public static char LivingToChar (Living thing) {
        switch (thing.who ()) {
            case BADGER:
                return 'B';
            case EMPTY:
                return 'E';
            case FOX:
                return 'F';
            case GRASS:
                return 'G';
            case RABBIT:
                return 'R';
            default:
                return 'E';
        }
    }

    /**
     * Instantiates a living object based off header data.
     *
     * @param charLiving
     * @param iRow
     * @param iColumn
     * @param iAge
     * @return Living object instantiated
     */
    public Living CharToLiving (char charLiving, int iRow, int iColumn, int iAge) {
        switch (charLiving) {
            case 'B':
                return new Badger (this.m_plain, iRow, iColumn, iAge);
            case 'F':
                return new Fox (this.m_plain, iRow, iColumn, iAge);
            case 'R':
                return new Rabbit (this.m_plain, iRow, iColumn, iAge);
            case 'G':
                return new Grass (this.m_plain, iRow, iColumn);
            case 'E':
                return new Empty (this.m_plain, iRow, iColumn);
            default:
                return new Empty (this.m_plain, iRow, iColumn);
        }
    }

    /**
     * Helper methods which instantiates a living object at age 0.
     *
     * @param charLiving
     * @param iRow
     * @param iColumn
     * @return Living object instantiated
     */
    public Living CharToLiving (char charLiving, int iRow, int iColumn) {
        return this.CharToLiving (charLiving, iRow, iColumn, 0);
    }

    /**
     * Helper method which breaks down a string to determine what object to
     * instantiate.
     *
     * @param strInput
     * @param iRow
     * @param iColumn
     * @return Living object instantiated
     */
    public Living ToLiving (String strInput, int iRow, int iColumn) {
        final boolean bAgeSpecified = strInput.length () > 1;
        final char charLiving = strInput.charAt (0);
        final int iAge = bAgeSpecified ? Integer.parseInt (strInput.substring (1, strInput.length ())) : 0;

        Living thing = this.CharToLiving (charLiving, iRow, iColumn);

        if (thing instanceof Animal)
            thing = this.CharToLiving (charLiving, iRow, iColumn, iAge);

        return thing;
    }

    /**
     * Parses given Scanner buffer into a valid 2D living array.
     *
     * @param file
     * @return Formatted 2D Living Array
     */
    public ArrayList<ArrayList<Living>> FileImport (Scanner file) {
        final ArrayList<ArrayList<Living>> arrGrid = new ArrayList<> ();
        ArrayList<Living> arrRow;
        int iColumn = 0;

        while (file.hasNextLine ()) {
            arrRow = new ArrayList<> ();

            final Scanner line = new Scanner (file.nextLine ());

            while (line.hasNext ()) {
                final String strIn = line.next ();
                final Living thing = this.ToLiving (strIn, arrGrid.size (), iColumn);
                arrRow.add (thing);
                iColumn ++;
            }
            arrGrid.add (arrRow);

            iColumn = 0;
            line.close ();
        }

        file.close ();

        return arrGrid;
    }

    /**
     * Serializes a grid into the String variant.
     *
     * @param arrLivingGrid
     * @return Serialized String
     */
    public static String Export (Living[][] arrLivingGrid) {
        final StringBuilder build = new StringBuilder ();

        for (int iRow = 0; iRow < arrLivingGrid.length; iRow ++) {
            for (int iColumn = 0; iColumn < arrLivingGrid [iRow].length; iColumn ++) {
                // Append Type
                final Living thing = arrLivingGrid [iRow] [iColumn];

                build.append (LivingToChar (thing));
                if (thing instanceof Animal) {
                    // Append age of animal
                    build.append (((Animal) thing).myAge ());
                    build.append (" ");
                } else
                    build.append ("  ");
            }
            build.append ("\r\n");
        }

        return build.toString ();
    }

    /**
     * Alias to Export, used in dynamic form.
     *
     * @return Serialized String
     */
    public String Export () {
        return Export (this.m_plain.grid);
    }
}