package edu.iastate.cs228.hw1;

/**
 * Badger
 *
 * Usage: A badger eats a rabbit and competes against a fox.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class Badger extends Animal {
    /**
     * Constructor
     *
     * @param p: plain
     * @param r: row position
     * @param c: column position
     * @param a: age
     */
    public Badger( Plain p, int r, int c, int a ) {
        super (p, r, c, a);
    }

    /**
     * A badger occupies the square.
     */
    @Override
    public State who () {
        return State.BADGER;
    }

    /**
     * A badger dies of old age or hunger, or from isolation and attack by a group
     * of foxes.
     *
     * @return Living life form occupying the square in the next cycle.
     */
    @Override
    public Living next (Plain pNew) {
        final int[] arrResults = this.CensusArray ();
        this.census (arrResults);

        // Rule A
        if (this.age >= BADGER_MAX_AGE)
            return new Empty (pNew, this.row, this.column);

        // Rule B
        if (arrResults [BADGER] == 1 && arrResults [FOX] > 1)
            return new Fox (pNew, this.row, this.column, 0);

        // Rule C
        if ((arrResults [BADGER] + arrResults [FOX]) > arrResults [RABBIT])
            return new Empty (pNew, this.row, this.column);

        return new Badger (pNew, this.row, this.column, this.age + 1);

    }
}
