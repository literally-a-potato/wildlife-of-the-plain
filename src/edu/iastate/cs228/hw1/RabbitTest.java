/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * RabbitTest
 *
 * Usage: Various tests for the Rabbit class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class RabbitTest {

    @Test
    public void TestbedAgeRabbit () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-AllGrass.txt");
        Living thing = new Rabbit (p, 1, 1, 0);
        p.grid [1] [1] = thing;

        thing = thing.next (p);
        assertEquals ("Rabbit should be one year of age as of now.", ((Animal) thing).myAge (), 1);

        for (int iAge = 0; iAge < Living.RABBIT_MAX_AGE; iAge ++)
            thing = thing.next (p);

        assertEquals ("The rabbit has been rendered an empty carcass.", State.EMPTY, thing.who ());
    }

    @Test
    public void TestbedNoFood () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-AllRabbits.txt");
        final Living thing = p.grid [1] [1].next (p);

        assertEquals ("The rabbit should be rendered an empty carcass.", State.EMPTY, thing.who ());
    }

    @Test
    public void TestbedEatenByFox () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-RabbitC.txt");
        final Living thing = p.grid [1] [1].next (p);

        assertEquals ("The rabbit should be replaced with a fox.", State.FOX, thing.who ());
    }

    @Test
    public void TestbedEatenByBadger () throws FileNotFoundException {
        final Plain p = new Plain ("templates/testbed-RabbitD.txt");
        final Living thing = p.grid [1] [1].next (p);

        assertEquals ("The rabbit should be replaced with a badger.", State.BADGER, thing.who ());
    }
}
