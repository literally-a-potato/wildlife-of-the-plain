package edu.iastate.cs228.hw1;

/**
 * Rabbit
 *
 * Usage: A rabbit eats grass and lives no more than three years.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public class Rabbit extends Animal {
    /**
     * Creates a Rabbit object.
     *
     * @param p: plain
     * @param r: row position
     * @param c: column position
     * @param a: age
     */
    public Rabbit( Plain p, int r, int c, int a ) {
        super (p, r, c, a);
    }

    // Rabbit occupies the square.
    @Override
    public State who () {
        return State.RABBIT;
    }

    /**
     * A rabbit dies of old age or hunger. It may also be eaten by a badger or a
     * fox.
     *
     * @return Living new life form occupying the same square
     */
    @Override
    public Living next (Plain pNew) {

        final int[] arrResults = this.CensusArray ();
        this.census (arrResults);

        // Rule A
        if (this.age >= Living.RABBIT_MAX_AGE)
            return new Empty (pNew, this.row, this.column);

        // Rule B
        if (arrResults [GRASS] == 0)
            return new Empty (pNew, this.row, this.column);

        // Rule C
        if ((arrResults [BADGER] + arrResults [FOX]) >= arrResults [RABBIT] && arrResults [BADGER] < arrResults [FOX])
            return new Fox (pNew, this.row, this.column, 0);

        // Rule D
        if (arrResults [BADGER] > arrResults [RABBIT])
            return new Badger (pNew, this.row, this.column, 0);

        return new Rabbit (pNew, this.row, this.column, this.age + 1);
    }
}
