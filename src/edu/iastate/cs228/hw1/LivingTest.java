/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertArrayEquals;

import java.io.FileNotFoundException;

import org.junit.Test;

/**
 * LivingTest
 *
 * Usage: Various tests for the Living class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class LivingTest {

    @Test
    public void TestbedCensus1 () throws FileNotFoundException {
        final Plain p = new Plain ("templates/packet-ext1.txt");
        final Living thing = p.grid [1] [1];

        final int[] arrResults = thing.CensusArray ();
        thing.census (arrResults);

        final int[] arrExpected = {
                1, 2, 0, 4, 2
        };

        assertArrayEquals ("Census results should be the same", arrExpected, arrResults);
    }

    @Test
    public void TestbedCensus2 () throws FileNotFoundException {
        final Plain p = new Plain ("templates/public1-3x3.txt");
        final Living thing = p.grid [1] [1];

        final int[] arrResults = thing.CensusArray ();
        thing.census (arrResults);

        final int[] arrExpected = {
                1, 1, 4, 2, 1
        };

        assertArrayEquals ("Census results should be the same", arrExpected, arrResults);
    }
}
