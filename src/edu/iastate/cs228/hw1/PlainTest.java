/**
 *
 */
package edu.iastate.cs228.hw1;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Test;

/**
 * PlainTest
 *
 * Usage: Various tests for the Plain class.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190131
 *
 */
public class PlainTest {

    @Test
    public void TestbedParse () throws FileNotFoundException {
        final Plain p = new Plain ("templates/public1-3x3.txt");

        assertEquals ("The width should be 3.", p.getWidth (), 3);

        final State[][] arrExpected = {
                {
                        State.GRASS, State.BADGER, State.FOX
                }, {
                        State.FOX, State.FOX, State.RABBIT
                }, {
                        State.FOX, State.EMPTY, State.GRASS
                }
        };

        for (int iRow = 0; iRow < p.grid.length; iRow ++)
            for (int iCol = 0; iCol < p.grid [iRow].length; iCol ++)
                assertEquals ("Each element should be the same type.", arrExpected [iRow] [iCol],
                        p.grid [iRow] [iCol].who ());
    }

    @Test
    public void TestbedRandomizer () throws FileNotFoundException {
        final Plain p = new Plain ("templates/public1-3x3.txt");
        final String strOriginal = p.toString ();
        p.randomInit ();
        final String strNew = p.toString ();

        assertNotEquals ("Randomizer should not return the same grid.", strOriginal, strNew);
    }

    @Test
    public void TestbedSave () throws Exception {
        final Plain p = new Plain ("templates/public1-3x3.txt");
        p.write ("TestbedSave.txt");
        final Plain p2 = new Plain ("TestbedSave.txt");
        Files.deleteIfExists (Paths.get ("TestbedSave.txt"));

        assertEquals ("Saving a file should yield similar results when serializing/re-parsing.", p.toString (),
                p2.toString ());

    }
}
