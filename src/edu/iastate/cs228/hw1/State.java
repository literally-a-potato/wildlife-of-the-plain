package edu.iastate.cs228.hw1;

/**
 * State
 *
 * Usage: Different forms of life.
 *
 * @author John Chandara <chandara@iastate.edu>
 * @license MIT License (X11 Variant)
 * @category Educational
 * @version 190209
 *
 */
public enum State {
    BADGER, EMPTY, FOX, GRASS, RABBIT
}